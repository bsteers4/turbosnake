<center>
# Turbo Snake
### Faster than the average snake 
<p>
(a work in progress)
</center>
<p>

### Thanks, attributions..

* Special thanks to Benoit Minisini for the wonderful gambas development environment.

Icon attributions..
* <a href="https://www.flaticon.com/free-icons/block" title="block icons">Block icons created by Smashicons - Flaticon</a>
* <a href="https://www.flaticon.com/free-icons/rocks" title="rocks icons">Rocks icons created by Dreamcreateicons - Flaticon</a>
* <a href="https://www.flaticon.com/free-icons/mouse" title="mouse icons">Mouse icons created by Freepik - Flaticon</a>
* Thanks for any other free imagery i have used.

### Features...
<p>

* Set 2 speeds, one normal and one turbo speed.

* Multiple players, Currently 3 key sets can be used. (I plan to add custom key editor)

* Fully customisable, Load custom images for all objects and main map, change Start length, growth length, number of Pills, Rocks, Money


### Instructions...
<p>
* Select player/players, then Goto the game page.

* Once all players have pressed a direction the game begins.

* Move around the map eating pills, grabbing money, catching mice.

* Avoid hitting rocks, your tail or another snake and not going off the map.

* Score is done by adding the grow length each time you eat a pill (mouse) or getting money scores 10x your snake length.

### Configuration...
<p>

* Block size can be set on the setting page. The play map size auto adjusts to the window size.

* Each player can have their own speed preference.

* Images can be any size as will be resized to fit.

* Snake images consist of 3 blocks, The image must be a rectangle 3x1 or 1x3 of any size (eg, 100x300), if  vertical the snake must point up, if horizontal the snake must point right.

#### Known bugs:

When a snake hits an obstacle (rock) the snake has been known to not quit when more than one player. I may have already fixed this.

#### Still Todo:
* Key editor, make custom control keysets.\
* Re-think scoring.\
* Be able to set block grid size like 100x100 blocks and the window adjust to it.

* Fully test/debug, i made this in a bit of a hurry before Christmas 2023 and have not fully tested all the features (mostly custom image stuff)
