<center>
# Turbo Snake
### Faster than the average snake 
<p>
(a work in progress)
</center>
<p>

### Thanks, attributions..

* Special thanks to Benoit Minisini for the wonderful gambas development environment.


### Things that might be of interest to gambas coders...

For this project i created the ImageGrid.class and Block.class.\
ImageGrid is a UserControl that can be added to any project.

It works by setting a Picture to use as the main image (map).\
You then set the block/grid size and add Blocks.\
Block.class creates blocks that are square pictures that can be painted to the map and have a Type integer setting.\
When a block is painted the ImageGrid1[X, Y] value is set.\
    ImageGrid1.PaintBlock(Block, Pos, iRotation, Colorize)
Pos is a 2 part array integer[] consisting of the X,Y position (in blocks not pixels)

You can get or set the type of any map point by using the ImageGrid array.

Eg.\
Print ImageGrid1[0,0]

map blocks can be removed and the background resored using ImageGrid.RestoreBlock(Pos)

That was all i needed to make a snake game but the class has other uses maybe?

